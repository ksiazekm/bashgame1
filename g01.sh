#!/bin/bash -

trap 'tput sgr0; tput cnorm; tput clear' EXIT
tput civis
stty -echo

width=$(tput cols)
height=$(tput lines)

trap 'yplane=$((height - 4))' WINCH

getrandom() {
	random=$(( 1 + $RANDOM % 100 ))
}

drawfence() {
	return 0
}

drawplane() {
	tput cup "$yplane" "$xplane"
	printf '%s' '  |'
	tput cup $((yplane + 1)) "$xplane"
	printf '%s' ' -|-'
	tput cup $((yplane + 2)) "$xplane"
	printf '%s' '--|--'
	tput cup $((yplane + 3)) "$xplane"
	printf '%s' ' -|-'
}

xplane=$((width * 50 / 100))
yplane=$((height - 4))

while sleep 0.1
do
	tput clear 
	width=$(tput cols)
	height=$(tput lines)
	getrandom
	x=$((random * $width / 100))
	getrandom
	y=$((random * $height / 100))
	while IFS= read -rs -t 0.001 -n 1 key
	do
		[[ $key == 'h' ]] && (( xplane--))
		[[ $key == 'l' ]] && (( xplane++))
	done
	drawplane
done
