#!/bin/bash -

trap 'tput clear; tput sgr0; tput cnorm; stty echo' EXIT

getrandom() {
	rand=$(($1 + RANDOM % $2))
}

drawplane() {
	tput cup "$yplane" "$xplane"
	printf '%s' '  |'
	tput cup $((yplane + 1)) "$xplane"
	printf '%s' ' -|-'
	tput cup $((yplane + 2)) "$xplane"
	printf '%s' '--|--'
	tput cup $((yplane + 3)) "$xplane"
	printf '%s' ' -|-'
}

resetgame() {
	for i in $(seq 0 3)
	do
		positions[$i * 4]=9999
	done	
}

xplane=40

tput civis
stty -echo

positions=()

resetgame


# row col dist length
# initialization of fences array with random data

while sleep 0.1
do
	tput clear
	width=$(tput cols)	
	height=$(tput lines)

	yplane=$((height - 4))

	for i in $(seq 0 3)
	do
		if [[ ${positions[$i * 4]} -lt $height ]]
		then
			((positions[i * 4]++))
		else
			positions[$i * 4]=0
			getrandom 1 $width
			positions[$i * 4 + 1]=$rand
			getrandom 1 10
			positions[$i * 4 + 2]=$rand
			getrandom 3 8
			positions[$i * 4 + 3]=$rand
		fi
		
#		printf '%i %i %i no=%i' "${positions[$i * 2]}" "${positions[$i * 2 + 1]}" "$height" "$i"
		if [[ ${positions[$i * 4 + 2]} -eq 0 ]]
		then
			tput cup ${positions[$i * 4]} ${positions[$i * 4 + 1]}
			for l in $(seq 1 ${positions[$i * 4 + 3]})
			do
				fence+='-'
			done

			printf '%s' "$fence "
			
		else
			((positions[$i * 4 + 2]--))
			positions[$i * 4]=0
		fi

		while IFS= read -rs -t 0.001 -n 1 key
		do
			[[ $key == 'h' ]] && (( xplane--))
			[[ $key == 'l' ]] && (( xplane++))
		done
		drawplane

		if [[ ${positions[$i * 4]} -ge $(($height - 4)) ]]
		then
			for col in $(seq 0 4) 
			do
				if (( $xplane + $col >= ${positions[$i * 4 + 1]} && $xplane + $col <= ${positions[$i * 4 + 1]} + ${positions[$i * 4 + 3]} )) 
				then
					tput clear
					go1="GAME OVER!!!"
					tput cup $(( (height - ${#go1}) / 2 )) $(( (width - ${#go1}) / 2 ))
					printf '%s\n' "$go1"
					go2="Press R to restart or Q to quit!"
					tput cup $(( (height - ${#go1}) / 2 + 1)) $(( (width - ${#go2}) / 2 ))
					printf '%s' "$go2"
					while IFS= read -rs -n 1 key
					do
						[[ $key == 'r' ]] && resetgame && break
						[[ $key == 'q' ]] && exit
					done

				fi
			done
		fi

		fence=$null
	done
done
